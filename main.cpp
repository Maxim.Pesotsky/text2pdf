
#include "HorriblePdfCreator.h"

#include <QApplication>

int main(int argc, char **argv)
{
  QApplication app(argc, argv);
  HorriblePdfCreator pdf(0);
  pdf.show();
  return app.exec();
}
