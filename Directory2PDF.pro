QT += gui widgets core printsupport
TEMPLATE = app
CONFIG += C++14

SOURCES += \
  HorriblePdfCreator.cpp \
  main.cpp

HEADERS += \
  HorriblePdfCreator.h

DISTFILES += \
  README.md

