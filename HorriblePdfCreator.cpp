﻿#include "HorriblePdfCreator.h"

#include <QPdfWriter>
#include <QDir>
#include <QDirIterator>
#include <QPainter>
#include <QImage>
#include <QString>
#include <QMainWindow>
#include <QDebug>
#include <QFileInfo>
#include <QFont>
#include <QFontMetrics>

#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QLinkedList>
#include <QMessageBox>
#include <QStringBuilder>

constexpr int DPI = 72;
constexpr int FontSize = 7;
constexpr int sizePerLine = 3 * FontSize / 2;

HorriblePdfCreator::HorriblePdfCreator(QWidget * parent) : QMainWindow(parent),
  fileNumber(0), lettersPerLine(80),
  nonEmptyLinesCount(0), brokenLine(0), sourceCodeSize(0), sourceCommentsSize(0), pageCount(0)
{
  this->setCentralWidget(new QWidget(this));
  QBoxLayout *pTopLayout = new QBoxLayout(QBoxLayout::TopToBottom);
  {
    QBoxLayout *phbxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    {
      QLabel *plbl = new QLabel("Input directory: ", this->centralWidget());
      ptxtInputDir = new QLineEdit("/home/max/git/dump", this);

      phbxLayout->addWidget(plbl);
      phbxLayout->addWidget(ptxtInputDir);
    }
    pTopLayout->addLayout(phbxLayout);

    phbxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    {
      QLabel *plbl = new QLabel("Files pattern: ", this->centralWidget());
      ptxtFilesPattern = new QLineEdit("cpp;h;cxx;hxx;c;cc;hh;js", this->centralWidget());

      phbxLayout->addWidget(plbl);
      phbxLayout->addWidget(ptxtFilesPattern);
    }
    pTopLayout->addLayout(phbxLayout);

    phbxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    {
      QLabel *plbl = new QLabel("Skip dirs:", this->centralWidget());
      ptxtDirAntiPattern = new QLineEdit("build;buildAsan;dist", this->centralWidget());

      phbxLayout->addWidget(plbl);
      phbxLayout->addWidget(ptxtDirAntiPattern);
    }
    pTopLayout->addLayout(phbxLayout);

    phbxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    {
      QLabel *plbl = new QLabel("Filter: ", this->centralWidget());
      ptxtFilterText = new QLineEdit(this);

      phbxLayout->addWidget(plbl);
      phbxLayout->addWidget(ptxtFilterText);
    }
    pTopLayout->addLayout(phbxLayout);

    phbxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    {
      QLabel *plbl = new QLabel("Output file: ", this->centralWidget());
      ptxtOutputFile = new QLineEdit("/home/max/test/doc_$n.pdf", this);

      phbxLayout->addWidget(plbl);
      phbxLayout->addWidget(ptxtOutputFile);
    }
    pTopLayout->addLayout(phbxLayout);

    phbxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    {
      pspnPartitionSize = new QSpinBox(this);
      pspnPartitionSize->setMinimum(1);
      pspnPartitionSize->setMaximum(16384);
      pspnPartitionSize->setValue(2040);

      phbxLayout->addWidget(new QLabel("Output file partition size: ", this->centralWidget()));
      phbxLayout->addWidget(pspnPartitionSize);
      phbxLayout->addWidget(new QLabel("MB", this->centralWidget()));
    }
    pTopLayout->addLayout(phbxLayout);

    phbxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    {
      pchkAliasing = new QCheckBox("Aliasing", this->centralWidget());
      //pchkAliasing->setChecked(true);
      pchkLossless = new QCheckBox("Lossless", this->centralWidget());
      pchkLossless->setChecked(true);

      phbxLayout->addWidget(pchkAliasing);
      phbxLayout->addWidget(pchkLossless);
    }
    pTopLayout->addLayout(phbxLayout);

    pbtnGenerate = new QPushButton("Generate", this->centralWidget());
    pTopLayout->addWidget(pbtnGenerate);

    HorriblePdfCreator::connect(pbtnGenerate, &QPushButton::clicked, this, &HorriblePdfCreator::generate);
  }
  this->centralWidget()->setLayout(pTopLayout);

}

HorriblePdfCreator::~HorriblePdfCreator()
{
  if (imgPainter && imgPainter->isActive())
    imgPainter->end();
  imgPainter.reset();
  if (pdfPainter && pdfPainter->isActive())
    pdfPainter->end();
  pdfPainter.reset();
  pdfWriter.reset();
}

bool HorriblePdfCreator::initNextPdfFile()
{
  // finish previos file, if was active
  if (pdfPainter && pdfPainter->isActive())
    pdfPainter->end();

  // get new file name
  ++fileNumber;
  QString filePath = ptxtOutputFile->text();
  if (0 <= filePath.indexOf(QLatin1String("$n"), 0, Qt::CaseSensitive))
  {
    filePath.replace(QLatin1String("$n"), QString::number(fileNumber));
  }
  else if (fileNumber > 1)
  {
    filePath = ptxtOutputFile->text() % QChar('.') % QString::number(fileNumber);
  }

  if (QFileInfo::exists(filePath))
  {
    QMessageBox msgBox;
    msgBox.setText(QLatin1String("File ") % filePath % QLatin1String(" exists.\n "));
    msgBox.setInformativeText("Do you want to cancel operation or discard existing file an rewrite it?");
    msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Discard);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    if (QMessageBox::Cancel == msgBox.exec())
      return false;
  }

  pdfWriter.reset(new QPdfWriter(filePath));

  pdfWriter->setPageSize(QPagedPaintDevice::A4);
  pdfWriter->setResolution(DPI);
  pdfWriter->setPageMargins(QMargins(30, 30, 30, 30));

  if (!pdfPainter)
    pdfPainter.reset(new QPainter);
  if (!pdfPainter->begin(pdfWriter.get()))
    return false;

  pdfPainter->setPen(Qt::black);
  pdfPainter->setFont(QFont("Times", FontSize));
  //pdfPainter->setRenderHint(QPainter::Antialiasing, pchkAliasing->isChecked());
  //pdfPainter->setRenderHint(QPainter::TextAntialiasing, pchkAliasing->isChecked());
  pdfPainter->setRenderHint(QPainter::LosslessImageRendering, pchkLossless->isChecked());

  if (!pdfFileInfo)
    pdfFileInfo.reset(new QFileInfo(filePath));
  else
    pdfFileInfo->setFile(filePath);

  if (!pdfFileInfo->exists())
    return false;

  return true;
}

bool HorriblePdfCreator::generate()
{
  if (ptxtOutputFile->text().isEmpty() || ptxtInputDir->text().isEmpty())
    return false;

  pbtnGenerate->setEnabled(false);
  nonEmptyLinesCount = 0;
  brokenLine = 0;
  sourceCodeSize = 0;
  sourceCommentsSize = 0;
  pageCount = 0;
  fileNumber = 0;

  if (!imgPainter)
    imgPainter.reset(new QPainter);

  if (!initNextPdfFile())
  {
    pbtnGenerate->setEnabled(true);
    return false;
  }

  QStringList filesPattern = ptxtFilesPattern->text().split(';', QString::SplitBehavior::KeepEmptyParts);
  QStringList dirsAntiPattern = ptxtDirAntiPattern->text().split(';', QString::SplitBehavior::SkipEmptyParts);
  for (auto it = dirsAntiPattern.begin(); it != dirsAntiPattern.end(); ++it)
    if (it->back() != '/')
      it->push_back('/');
  contentFilter= ptxtFilterText->text().split(';');

  // create image once (we init all pdf files the same)
  image = QImage(pdfPainter->viewport().size(), QImage::Format_Grayscale8);
  image.fill(Qt::white);
  if (imgPainter->begin(&image))
  {
    configureImagePainter();

    QDirIterator iter(ptxtInputDir->text(), QDirIterator::Subdirectories);
    QString ext, dir, filePath;
    while(iter.hasNext())
    {
      filePath = iter.next();
      QFileInfo finfo = iter.fileInfo();
      if (finfo.isFile())
      {
        dir = finfo.absoluteDir().path() + '/';
        ext = finfo.suffix();

        bool approved = filesPattern.empty();

        for (auto it = filesPattern.cbegin(); !approved && (it != filesPattern.cend()); ++it)
        {
          if (*it == ext)
            approved = true;
        }

        for (auto it = dirsAntiPattern.cbegin(); approved && (it != dirsAntiPattern.cend()); ++it)
        {
          approved = !dir.contains(*it);
        }

        if (approved)
        {
          if (writeFile(finfo))
            qDebug() << "Processed file: " << filePath;
          else
          {
            qDebug() << "Failed to write file: " << filePath;
            pbtnGenerate->setEnabled(true);
            return false;
          }
        }
        else
          qDebug() << "Skipped file: " << filePath;
      }
    }
    imgPainter->end();
  }
  pdfPainter->end();

  qDebug() << "pages: " << pageCount;
  qDebug() << "non empty lines: " << nonEmptyLinesCount;
  qDebug() << "broken lines: " << brokenLine;
  qDebug() << "code size: " << sourceCodeSize << " bytes";
  qDebug() << "comments removed: " << sourceCommentsSize << " bytes";

  pbtnGenerate->setEnabled(true);

  return true;
}

bool HorriblePdfCreator::writeFile(const QFileInfo &info)
{
  if (info.size() > (16u * 1024u * 1024u))  // 16MB of source is too much
    return false;

  QFile file(info.canonicalFilePath());
  if (!file.open(QIODevice::ReadOnly))
    return false;

  QString txt;
  {
    QByteArray arr = file.readAll();
    size_t fileSize = arr.size();
    if (!fileSize)
      return true;
    QLinkedList<TextBlock<QByteArray> > comments;
    comments.push_back(TextBlock<QByteArray>("//", "\n", "\n"));
    comments.push_back(TextBlock<QByteArray>("/*", "*/", ""));
    removeBlocks<QByteArray>(&arr, comments);
    size_t codeSize = arr.size();
    sourceCodeSize += codeSize;
    sourceCommentsSize += fileSize - codeSize;
    txt = QString::fromUtf8(arr);
  }

  if (txt.isEmpty())
    return true;

  int lBegin = 0;

  bool emptyLineLast = true;
  for (int i = 0; i < txt.size(); ++i)
  {
    if ('\n' == txt.at(i))
    {
      const int lEnd = i + 1;
      const bool emptyLine = (lEnd == (1 + lBegin));
      if (!emptyLine | !emptyLineLast)
      {
        if (!writeLine(txt.mid(lBegin, lEnd - lBegin - 1)))
          return false;
      }
      lBegin = lEnd;
      emptyLineLast = emptyLine;
    }
  }

  if (txt.size() > lBegin)
  {
    if (!writeLine(txt.mid(lBegin, txt.size() - lBegin)))
      return false;
  }
  flushPage(true);

  return true;
}

bool HorriblePdfCreator::writeLine(const QString &txt)
{
  if (lettersPerLine >= txt.size())
    return writeSubLine(txt);

  for (int i = 0; i < txt.size(); )
  {
    int subLineLenght = lettersPerLine;
    if ((i + subLineLenght) < txt.size())
    {
      QChar ch[4] = {0, 0, 0, 0};
      if ((i + subLineLenght) < txt.size())
        ch[2] = txt[i + subLineLenght];
      // search for position to break line (on some special symbol)
      ++brokenLine;
      for (int j = subLineLenght - 1; j > 1; --j)
      {
        ch[3] = ch[2];
        ch[2] = ch[1];
        ch[1] = ch[0];
        ch[0] = txt[i+j];

        // no '*' or '&' because of pointer operations
        if ((// first list of searchable patterns
            (' ' == ch[1])|(';' == ch[1])|(',' == ch[1])
            |(':' == ch[1])|('?' == ch[1])|('(' == ch[1])
            |('+' == ch[1])|('-' == ch[1])|('/' == ch[1])
            |('|' == ch[1])|('^' == ch[1])|('%' == ch[1])
            |('>' == ch[1])|('<' == ch[1])|('=' == ch[1]))
            // then list of combinations that must not be separated
            &(('(' != ch[1])|(')' != ch[2]))
            &(('-' != ch[1])|('>' != ch[2]))
            &(('/' != ch[1])|('*' != ch[2]))
            &(('*' != ch[1])|('/' != ch[2]))
            &(('/' != ch[1])|('/' != ch[2]))
            &(('|' != ch[1])|('|' != ch[2]))
            &(('=' != ch[1])|('=' != ch[2]))
            &(('>' != ch[1])|('=' != ch[2]))
            &(('<' != ch[1])|('=' != ch[2]))
            &(('.' != ch[1])|('.' != ch[2]))
            &(('[' != ch[1])|(']' != ch[2]))
            &(('{' != ch[1])|('}' != ch[2]))
            )
        {
          // ch[0..1] will be left on previous line
          // ch[2..3] will move to next line
          subLineLenght = j + 2;
          --brokenLine;
          break;
        }
      }
      // if never found, break just at lettersPerLine
    }

    if (!writeSubLine(txt.mid(i, subLineLenght)))
      return false;
    i += subLineLenght;
  }

  //pdfPainter->drawText(printArea, Qt::AlignLeft, txt);

  return true;
}

bool HorriblePdfCreator::writeSubLine(const QString& txt)
{
  if ((printArea.height() < sizePerLine) && !flushPage(true))
    return false;

  nonEmptyLinesCount += txt.size() > 2;
  imgPainter->drawText(printArea, txt);
  printArea.moveTop(printArea.top() + sizePerLine);
  printArea.setHeight(printArea.height() - sizePerLine);
  return true;
}

bool HorriblePdfCreator::flushPage(bool addPage)
{
  ++pageCount;
  pdfPainter->drawImage(pdfPainter->viewport(), image);
  if (addPage)
  {
    if ((!pdfFileInfo || !pdfFileInfo->exists()) && !initNextPdfFile())
      return false;

    pdfFileInfo->refresh();
    if (pdfFileInfo->size() > ((size_t)(pspnPartitionSize->value())*1024u*1024u))
    {
      if (!initNextPdfFile())
        return false;
    }
    else if (!pdfWriter->newPage())
      return false;

    if (imgPainter->isActive())
      imgPainter->end();
    image.fill(Qt::white);
    if (!imgPainter->begin(&image))
      return false;
    configureImagePainter();
  }
  return true;
}

template<class T>
static bool equal(const T& s1, int p1, const T& s2, int p2, int n)
{
  if (((p2 + n) > s2.size())
     |((p1 + n) > s1.size()))
    return false;
  for (int i = 0; i < n; ++i)
  {
    if (s1.at(p1 + i) != s2.at(p2 + i))
      return false;
  }
  return true;
}

template<class T>
void HorriblePdfCreator::removeBlocks(T* txt, const QLinkedList<TextBlock<T> >& blocks)
{
  T upd;

  int pos = 0;
  int posNextLast = 0;
  while (pos < txt->size())
  {
    int posNext = -1;
    const TextBlock<T>* block = nullptr;
    for (auto it = blocks.cbegin(); it != blocks.cend(); ++it)
    {
      if (equal(it->start, 0, *txt, pos, it->start.size()))
      {
        block = &*it;
        break;
      }
    }
    if (block)
    {
      posNext = txt->indexOf(block->end, pos + block->start.size());
      if (posNext > pos)
      {
        posNext += block->end.size();
        if ((upd.isEmpty()) & (pos > 0))
          upd.reserve(txt->size() - (posNext - pos) + block->repl.size());
        upd.append(txt->constData() + posNextLast, pos - posNextLast);
        upd += block->repl;
        pos = posNext;
        posNextLast = posNext;
      }
      else
        ++pos;
    }
    else
      ++pos;
  }

  // last part
  upd.reserve(upd.size() + pos - posNextLast);
  upd.append(txt->constData() + posNextLast, pos - posNextLast);

  if (!upd.isEmpty())
    txt->swap(upd);
  return;
}

void HorriblePdfCreator::configureImagePainter()
{
  if (!imgPainter || !imgPainter->isActive())
    return;

  imgPainter->setPen(Qt::black);

  QFont font("Monospace", FontSize);
  font.setStyleHint(QFont::Monospace);
  //font.setStyleHint(QFont::TypeWriter);

  imgPainter->setFont(font);
  imgPainter->setRenderHint(QPainter::Antialiasing, pchkAliasing->isChecked());
  imgPainter->setRenderHint(QPainter::TextAntialiasing, pchkAliasing->isChecked());

  printArea = imgPainter->viewport();

  lettersPerLine  = (uint16_t)(printArea.width() / imgPainter->fontMetrics().horizontalAdvance("X"));

  return;
}














