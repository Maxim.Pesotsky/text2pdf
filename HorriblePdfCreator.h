#ifndef HORRIBLEPDFCREATOR_H
#define HORRIBLEPDFCREATOR_H

#include <memory>

#include <QString>
#include <QMainWindow>
#include <QRect>
#include <QImage>

class QWidget;
class QPdfWriter;
class QPainter;
class QFileInfo;
class QLabel;
class QLineEdit;
class QCheckBox;
class QSpinBox;
class QPushButton;

class HorriblePdfCreator : public QMainWindow
{
public:
  explicit HorriblePdfCreator(QWidget * parent);
  virtual ~HorriblePdfCreator();

  bool generate();

private:
  std::unique_ptr<QPdfWriter> pdfWriter;
  std::unique_ptr<QPainter> pdfPainter;
  std::unique_ptr<QPainter> imgPainter;
  std::unique_ptr<QFileInfo> pdfFileInfo;
  QRect printArea;
  QImage image;
  QStringList contentFilter;

  QLineEdit *ptxtInputDir;
  QLineEdit *ptxtOutputFile;
  QSpinBox *pspnPartitionSize;
  QLineEdit *ptxtFilesPattern;
  QLineEdit *ptxtDirAntiPattern;
  QLineEdit *ptxtFilterText;
  QCheckBox *pchkLossless;
  QCheckBox *pchkAliasing;
  QCheckBox *pchkRemoveComments;
  QPushButton *pbtnGenerate;

  uint32_t fileNumber;
  uint16_t lettersPerLine;

  // statistics
  size_t nonEmptyLinesCount;
  size_t brokenLine;
  size_t sourceCodeSize;
  size_t sourceCommentsSize;
  size_t pageCount;

  bool writeLine(const QString& txt);
  bool writeSubLine(const QString& txt);
  bool writeFile(const QFileInfo& info);

  bool flushPage(bool addPage);
  bool initNextPdfFile();

  void configureImagePainter();

  template<class T> struct TextBlock { T start, end, repl; TextBlock(const T& start, const T& end, const T& repl) : start(start), end(end), repl(repl) {} };
  template<class T> void removeBlocks(T* txt, const QLinkedList<TextBlock<T> >& blocks);
};

#endif // HORRIBLEPDFCREATOR_H
